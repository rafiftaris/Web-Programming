from django.shortcuts import render,redirect
from .models import Schedule
from .forms import Schedule_Form

# Create your views here.
def index(request):
    return render(request,'Story03WelcomePage.html',{})

def mainpage(request):
	return render(request,'Story03.html',{})	

def registration(request):
	return render(request,'Story03Challenge.html',{})

# def schedule_form(request):
# 	return render(request,'ScheduleForm.html',{})

def schedule_form(request):
	form = Schedule_Form(request.POST or None)
	response = {}
	if (request.method == 'POST'):
		if(form.is_valid()):
			response['event'] = request.POST.get("event")
			response['date'] = request.POST.get("date")
			response['time'] = request.POST.get("time")
			response['venue'] = request.POST.get("venue")
			response['category'] = request.POST.get("category")
			schedule = Schedule(date=response['date'],time=response['time'],event=response['event'],venue=response['venue'],category=response['category'])
			schedule.save()
	
	response['form'] = form
	return render(request,"ScheduleForm.html", response)		
			


def schedule_view(request):
	schedule = Schedule.objects.all()
	response = {
		"jadwal" : schedule
	}
	return render(request, 'ScheduleView.html', response)

def schedule_reset(request):
	Schedule.objects.all().delete()
	return render(request, 'ScheduleView.html', {})