from django import forms

class Schedule_Form(forms.Form):

	date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
	time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs={'type' : 'time'}))
	event = forms.CharField(label='Event', required=True, max_length=25)
	venue = forms.CharField(label='Venue', required=True, max_length=50)
	category = forms.CharField(label='Category', required=True, max_length=10)