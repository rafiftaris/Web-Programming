from django.urls import path
from .views import *

app_name = 'mysite'
urlpatterns = [
	path('',index, name='index'),
	path('main/',mainpage, name='mainpage'),
    path('registration/',registration, name='registration'),
    path('schedule_form/',schedule_form,name='schedule_form'),
    path('schedule_reset/',schedule_reset,name='schedule_reset'),
    path('schedule/',schedule_view, name='schedule_view'),
]