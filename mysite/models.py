from django.db import models
from datetime import date,time

# Create your models here.
class Schedule(models.Model):
	date = models.DateField()
	time = models.TimeField()
	event = models.CharField(max_length=25)
	venue = models.CharField(max_length=50)
	category = models.CharField(max_length=10)